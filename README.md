# scraven2/config/bash
My custom bash configuration

## Install
Clone and symlink or install with [ellipsis][ellipsis]:

```
$ ellipsis install gitlab.com/scraven2/config/bash
```

[ellipsis]: http://ellipsis.sh
