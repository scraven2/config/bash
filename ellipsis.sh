#!/usr/bin/env bash

### TODO
#     - maybe add a pkg.uninstall hook to remove the .bashrc lines?

pkg.install() {
    MARKER="### scraven2/config/bash"

    if ! cat ~/.bashrc | grep -q "$MARKER"
    then
      echo >> ~/.bashrc
      echo "$MARKER" >> ~/.bashrc
      echo "SHELL=\"${PKG_PATH}/shell.bash\"" >> ~/.bashrc
      echo "[ -f \$SHELL ] && source \$SHELL" >> ~/.bashrc
      echo >> ~/.bashrc
    fi
}

pkg.link() {
  # overriding this hook to prevent any linking
  # we don't want to do anything here
  # we'll just use the init hook to source our config
  true
}

