#!/bin/bash
######### TODO
#
# ### Completions
#     - docker exec <tab> complete running containers
#
# ### docker
#     - persist history between runs/execs of containers with same name
#     - ?? mount bash shell into container?
#

# # Source Ellipsis init code
# ELLIPSIS_INIT="/home/shannon/repos/github.com/ellipsis/ellipsis/init.sh"
# [ -f "$ELLIPSIS_INIT" ] && source "$ELLIPSIS_INIT"

function command_not_found_handle() {
  if [ -L "${HOME}/shortcuts/dirs/$1" ]
  then
    edit $1
  fi

  echo "Unknown Command: $1" >&2
  return 1
}

FOLDER=" "
CHEVRON=""
BOLT=""
GIT=""
GIT2=" "
HOME_ICON=" "
GITHUB_FOLDER2=" "
GITHUB_FOLDER=" "
DOUBLE_CHECK="󰄭 "
BOX_CHECK=" "
CHECK="$BOX_CHECK"
TERMINAL1=" " 
TERMINAL2=" "
TERMINAL=$TERMINAL1
GITLAB1=" "
GITLAB=$GITLAB1

# https://dev.to/ifenna__/adding-colors-to-bash-scripts-48g4
CYAN=$(echo -e "\e[36m")      # cyan
PURPLE=$(echo -e "\e[35m")    # purple
GRAY=$(echo -e "\e[90m")      # gray
GREEN=$(echo -e "\e[32m")     # green
RED=$(echo -e "\e[31m")       # red
RESET=$(echo -e "\e[0m")


COLOR_LINE1=$GRAY
COLOR_LINE2=$GRAY
COLOR_PROMPT=$GREEN

MARGIN_LEFT=$(printf '%*s' 5)

function green {
  echo -e "\e[32m$@\e[0m"
}


function red {
  echo -e "\e[31m$@\e[0m"
}

function folder_icon() {
  if pwd | grep -q "$HOME"
  then
    echo $HOME_ICON
  else
    echo $FOLDER
  fi
}

function shorten_dirs {
  [ -z "$PWD_KEEP" ] && PWD_KEEP=3
  
  if [ $PWD_KEEP -lt 1 ]
  then
    echo $1
    return
  fi

  RESULT=""
  echo $1 | rev | tr '/' '\n' | (while read D
  do 
    PWD_KEEP=$(($PWD_KEEP-1))
    if [ $PWD_KEEP -lt 0 ]
    then 
      D=$(echo $D | rev | cut -c1)
    fi

    RESULT="${RESULT}/${D}"
  done
  
  echo $RESULT | sed 's@/@@' | rev
  )
}

function my_pwd() {
  ICON=$FOLDER
  DIR=$(pwd)

  if pwd | grep -Eq "^$HOME"
  then
    ## override ICON to the little house 
    ICON=$HOME_ICON

    ## and shorten DIR
    DIR=$(echo $DIR | sed "s@${HOME}/@@")
  fi

  if echo $DIR | grep -Eq "^repos/github.com"
  then
    ICON=$GITHUB_FOLDER
    DIR=$(echo $DIR | sed "s@repos/github.com/@@")
  fi

  if echo $DIR | grep -Eq "^repos/gitlab.com"
  then
    ICON=$GITLAB
    DIR=$(echo $DIR | sed "s@repos/gitlab.com/@@")
  fi

  echo "${ICON} $(shorten_dirs $DIR)"
  ## shorten the names of the directories to just their initial
  # INITIALS=$((echo $DIR | rev | cut -d'/' -f4- | rev | tr '/' '\n' | while read D; do echo $D | cut -c1; done;) | tr '\n' '/' | sed 's@/@@')
  ## except for the tail end, keep those full length
  # TAIL=$(echo $DIR | rev | cut -d'/' -f1-3 | rev | sed 's@/@@')

  # echo " ${ROOT_SLASH}${INITIALS}${SLASH}${TAIL}"
}

# function prompt_right() {
#   # echo -e "\033[0;35m\\\t\033[0m"
#   echo -e "\e[35m \033[0;90m\\\t\033[0m"
# }
#
# function prompt_left() {
#   # echo -e "\033[0;90m.....\w\033[0m"
#   echo -e "\033[0;90m $(my_pwd) \t ..... \033[0m"
# }
#
# function prompt() {
#     # compensate=1
#     compensate=10
#     PS1=$(printf "\n%*s\r%s\n\e[36m ${CHEVRON}${CHEVRON} " "$(($(tput cols)+${compensate}))" "$(prompt_right)" "$(prompt_left)")
#     DOTS=$(printf "")
# }

function line1 {
  COLS=$(tput cols)
  HALF=$(($COLS/2))
  QTR=$(($HALF/2))
  EIGTH=$(($QTR/2))

  # LEFT=$(printf '%*s' $EIGTH)
  DOTS=$(printf '.%.0s' $(seq 1 $(($HALF+$QTR+$EIGTH-5))))
  RIGHT=$(printf '%*s' $EIGTH)

  START=$(date "+%s" -d "$(history | tail -n1 |  history | tail -n1 | awk '{print $2" "$3}')")
  NOW=$(date "+%s")
  DURATION=$(($NOW-$START))

  RIGHT=""

  DUR=""
  if [ $DURATION -gt 3 ]
  then
    DUR="${DURATION}s"
  fi

  STATUS="${GREEN}${CHECK}${RESET}"
  if [ $LAST_EXIT_CODE -ne 0 ]
  then
    STATUS="$(red $LAST_EXIT_CODE)"
  fi

  # DUR="12345s"
  # STATUS="$(red 1234)"
  RIGHT="${STATUS}  ${DUR}"
  echo -n "${MARGIN_LEFT}${COLOR_LINE1}${DOTS}  ${RIGHT}${RESET}"
}

function line2 {
  SPACER="  "
  [ "$SSH_TTY" ] && HOST="${TERMINAL} $(cat /etc/hostname)${SPACER}"

  echo -n " ${MARGIN_LEFT}${COLOR_LINE2}${HOST}$(my_pwd)"
}

function new_prompt() {
  COLS=$(tput cols)

  DOTS=$(printf '.%.0s' $(seq 1 $(tput cols)))
  PS1="$(line1)\n$(line2)\n${COLOR_PROMPT} ${CHEVRON}${CHEVRON}  "
}
PROMPT_COMMAND=new_prompt

function hook_pre_exec() {
  export LAST_EXIT_CODE=$?
  echo -e "\e[0m"
}

export HISTTIMEFORMAT="%F %T "
trap hook_pre_exec DEBUG

